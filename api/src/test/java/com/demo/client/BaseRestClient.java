package com.demo.client;

import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Value;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class BaseRestClient {
    
    @Value("${api.url}")
    protected String baseUrl;
    @Value("${api.key}")
    private String key;

    public RequestSpecification request() {
        return given().baseUri(baseUrl).contentType(JSON).header("api_key", key);
    }
}
