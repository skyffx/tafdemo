package com.demo.client;

import app.petstore.dto.*;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.springframework.stereotype.Service;

@Service
public class PetClient extends BaseRestClient {
    public static final String PET_URL = "/pet";

    @Step("Add pet")
    public Pet addPet(Pet pet) {
        return request()
                .body(pet)
                .post(PET_URL)
                .then().statusCode(200)
                .extract().response().as(Pet.class);
    }

    @Step("Find pet by id {0}")
    public Response findPetById(Long petId) {
        return request()
                .get(PET_URL + "/" + petId)
                .then().extract().response();
    }

    @Step("Delete pet by id {0}")
    public Response deletePetById(Long petId) {
        return request()
                .delete(PET_URL + "/" + petId)
                .then().extract().response();
    }

    @Step("Update pet")
    public Pet updatePet(Pet pet) {
        return request()
                .body(pet)
                .put(PET_URL)
                .then().statusCode(200)
                .extract().response().as(Pet.class);
    }

    @Step("Get base url")
    public String getUrl() {
        return baseUrl;
    }
}
