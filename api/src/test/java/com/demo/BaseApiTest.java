package com.demo;

import com.demo.config.ContextConfig;
import io.restassured.RestAssured;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeSuite;

@ContextConfiguration(classes = ContextConfig.class)
public class BaseApiTest extends AbstractTestNGSpringContextTests {
    
    @BeforeSuite
    public void beforeAll() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
