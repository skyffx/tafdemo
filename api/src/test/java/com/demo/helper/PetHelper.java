package com.demo.helper;

import app.petstore.dto.*;
import com.github.javafaker.Faker;
import io.qameta.allure.Step;

import java.util.Collections;

public class PetHelper {
    private static final Faker faker = new Faker();

    private PetHelper() {}

    @Step("Create dummy pet")
    public static Pet getDummyPet() {
        String name = faker.animal().name();
        return new Pet()
                .name(name)
                .category(new Category().name("jumper"))
                .tags(Collections.singletonList(new Tag().name(name)))
                .status(Pet.StatusEnum.AVAILABLE);
    }
}
