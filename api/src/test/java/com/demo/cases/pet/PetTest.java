package com.demo.cases.pet;

import app.petstore.dto.*;
import com.demo.BaseApiTest;
import com.demo.client.PetClient;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static com.demo.client.PetClient.PET_URL;
import static java.lang.Long.parseLong;
import static org.assertj.core.api.Assertions.assertThat;
import static com.demo.helper.PetHelper.getDummyPet;

@Feature("Pet Controller")
public class PetTest extends BaseApiTest {
    
    @Autowired
    private PetClient petClient;

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("User cannot access endpoint with self-defined http method")
    public void requestMethodNotAllowed() {
        petClient.request()
                .request("TEST", petClient.getUrl() + PET_URL)
                .then().statusCode(405);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("User cannot access requested resource")
    public void requestedResourceDoesNotExist() {
        ModelApiResponse response = petClient.request()
                .get(petClient.getUrl() + "/12345")
                .then().extract().as(ModelApiResponse.class);
        assertThat(response.getMessage()).containsIgnoringCase("null for uri");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Add new pet and found it by correct id")
    public void newPetShouldBeFoundByCorrectId() {
        Long petId = petClient.addPet(getDummyPet()).getId();
        Response response = petClient.findPetById(petId);
        assertThat(response.statusCode()).isEqualTo(200);
        assertThat(response.getBody().as(Pet.class).getId()).isEqualTo(petId);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Pet cannot be found by incorrect id")
    public void petCannotBeFoundByIncorrectId() {
        Long petId = petClient.addPet(getDummyPet()).getId();
        Response response = petClient.findPetById(parseLong(StringUtils.chop(petId.toString())));
        assertThat(response.statusCode()).isEqualTo(404);
        assertThat(response.getBody().as(ModelApiResponse.class).getMessage()).isEqualTo("Pet not found");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Delete pet by correct id")
    public void deletePetByCorrectId() {
        Long petId = petClient.addPet(getDummyPet()).getId();
        Response response = petClient.deletePetById(petId);
        assertThat(response.statusCode()).isEqualTo(200);
        assertThat(response.getBody().as(ModelApiResponse.class).getMessage()).isEqualTo(String.valueOf(petId));
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Pet cannot be deleted by incorrect id")
    public void petCannotBeDeleteByIncorrectId() {
        Long petId = petClient.addPet(getDummyPet()).getId();
        Response response = petClient.deletePetById(parseLong(StringUtils.chop(petId.toString())));
        assertThat(response.statusCode()).isEqualTo(404);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Update existing pet found by id")
    public void updateExistingPetFoundById() {
        Long petId = petClient.addPet(getDummyPet()).getId();
        Pet currentPet = petClient.findPetById(petId).getBody().as(Pet.class);
        Pet updatedPet = getDummyPet();
        updatedPet.setId(petId);
        petClient.updatePet(updatedPet);
        assertThat(currentPet).isNotSameAs(updatedPet);
        assertThat(petClient.findPetById(petId).getBody().as(Pet.class).getName()).isEqualTo(updatedPet.getName());
    }
}
