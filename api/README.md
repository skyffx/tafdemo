# API

API Test Automation Demo

### Run test
- `cd api`
- `mvn clean compile`
- `mvn -Dtest=PetTest clean test`

### Generate report
- `mvn allure:serve`

</br><p align="center">
	<img src="report.png" />
</p>