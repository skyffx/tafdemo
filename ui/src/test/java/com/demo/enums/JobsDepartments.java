package com.demo.enums;

import lombok.Getter;

public enum JobsDepartments {
    QUALITY_ASSURANCE("Quality Assurance");

    @Getter
    private final String value;

    JobsDepartments(String value) {
        this.value = value;
    }
}
