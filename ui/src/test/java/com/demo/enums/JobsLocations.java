package com.demo.enums;

import lombok.Getter;

public enum JobsLocations {
    ISTANBUL_TURKEY("Istanbul, Turkey");

    @Getter
    private final String value;

    JobsLocations(String value) {
        this.value = value;
    }
}
