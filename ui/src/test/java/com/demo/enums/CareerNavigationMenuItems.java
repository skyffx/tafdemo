package com.demo.enums;

import lombok.Getter;

public enum CareerNavigationMenuItems {
    CULTURE("CULTURE"),
    LOCATIONS("LOCATIONS"),
    TEAMS("TEAMS"),
    JOBS("JOBS"),
    LIFE_AT_INSIDER("LIFE AT INSIDER");

    @Getter
    private final String value;

    CareerNavigationMenuItems(String value) {
        this.value = value;
    }
}
