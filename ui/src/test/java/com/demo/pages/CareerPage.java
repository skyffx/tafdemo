package com.demo.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

import java.util.stream.IntStream;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.demo.enums.CareerNavigationMenuItems.*;

public class CareerPage {
    private final ElementsCollection careerNavigationMenuItems = $$("#career-nav .col").filterBy(visible);
    private final ElementsCollection jobsLocation = $$(".jobs-locations option").filterBy(visible);
    private final ElementsCollection jobsDepartments = $$(".jobs-teams option").filterBy(visible);
    private final ElementsCollection jobsList = $$(".jobs-list a").filterBy(visible);

    @Step("Go to '{0}' section")
    public CareerPage gotoSection(String sectionName) {
        careerNavigationMenuItems.findBy(text(sectionName)).click();
        $(String.format("#%s", sectionName.toLowerCase().replace(" ", "-"))).shouldBe(visible);
        $("#to-top").click();
        Selenide.sleep(2000);
        return this;
    }

    @Step("Verify 'Career' page sections")
    public CareerPage verifyCareerSections() {
        gotoSection(CULTURE.getValue());
        gotoSection(LOCATIONS.getValue());
        gotoSection(TEAMS.getValue());
        gotoSection(JOBS.getValue());
        gotoSection(LIFE_AT_INSIDER.getValue());
        return this;
    }

    @Step("Filter jobs by location '{0}' and department '{1}'")
    public CareerPage filterJobsByLocationAndDepartment(String location, String department) {
        gotoSection(JOBS.getValue());
        jobsLocation.findBy(text(location)).click();
        jobsDepartments.findBy(text(department)).click();
        $(".jobs-list").shouldBe(visible);
        Selenide.sleep(2000);
        return this;
    }

    @Step("Verify filtered jobs contain location '{0}' and department '{1}'")
    public CareerPage verifyFilteredJobsContainLocationAndDepartment(String location, String department) {
        IntStream.range(0, jobsList.size()).forEach(i -> {
            jobsList.get(i).$(".job-title").shouldHave(text(department));
            jobsList.get(i).$$(".tag").get(0).shouldHave(text(department));
            jobsList.get(i).$$(".tag").get(1).shouldHave(text(location));
        });
        return this;
    }

    @Step("Select first available job offer")
    public CareerPage selectFirstAvailableJobOffer() {
        jobsList.get(0).click();
        return this;
    }
}
