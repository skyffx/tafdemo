package com.demo.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class JobOfferPage {
    private final SelenideElement jobTitle = $(".posting-headline h2");
    private final ElementsCollection jobData = $$(".page-centered").filterBy(visible);
    private final SelenideElement jobApplicationButton = $(byText("Apply for this job"));

    @Step("Verify job offer page is open")
    public JobOfferPage verifyPageIsOpen() {
        jobApplicationButton.shouldBe(visible);
        return this;
    }

    @Step("Verify job title")
    public JobOfferPage verifyTitle(String title) {
        jobTitle.shouldHave(text(title)).shouldBe(visible);
        return this;
    }

    @Step("Verify job description")
    public JobOfferPage verifyDescription() {
        jobData.findBy(text("Insider")).shouldBe(visible);
        return this;
    }

    @Step("Verify job requirements")
    public JobOfferPage verifyRequirements() {
        jobData.findBy(text("Requirements")).shouldBe(visible);
        return this;
    }

    @Step("Verify 'Apply for this job' button and click")
    public JobOfferPage verifyApplicationButtonAndClick() {
        jobApplicationButton.shouldBe(visible).click();
        return this;
    }
}
