package com.demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class JobApplicationPage {
    private final SelenideElement submitApplicationButton = $("[type=submit]");

    @Step("Verify job application page is open")
    public JobApplicationPage verifyJobApplicationPageIsOpen() {
        $(byText("Submit your application")).shouldBe(visible);
        submitApplicationButton.shouldBe(visible);
        return this;
    }
}
