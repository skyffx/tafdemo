package com.demo.pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MainPage {
    private final ElementsCollection mainNavigationMenuItems = $$(".ubermenu-item-layout-text_only").filterBy(visible);

    @Step("Verify main page is open")
    public MainPage verifyMainPageIsOpen() {
        $("[alt=Insider]").shouldBe(visible);
        return this;
    }

    @Step("Open 'Career' page")
    public MainPage openCareerPage() {
        mainNavigationMenuItems.findBy(text("CAREER")).click();
        return this;
    }
}
