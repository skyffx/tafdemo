package com.demo.cases.e2e;

import com.demo.BaseUITest;
import com.demo.pages.CareerPage;
import com.demo.pages.JobApplicationPage;
import com.demo.pages.JobOfferPage;
import com.demo.pages.MainPage;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.demo.enums.JobsDepartments.*;
import static com.demo.enums.JobsLocations.*;

@Feature("Jobs")
public class JobApplicationTest extends BaseUITest {
        
    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Should apply for Quality Assurance job located in Istanbul, Turkey")
    public void applyForQualityAssuranceJobTest() {
        openInsiderMainPage();

        new MainPage()
                .verifyMainPageIsOpen()
                .openCareerPage();

        new CareerPage()
                .verifyCareerSections()
                .filterJobsByLocationAndDepartment(ISTANBUL_TURKEY.getValue(), QUALITY_ASSURANCE.getValue())
                .verifyFilteredJobsContainLocationAndDepartment(ISTANBUL_TURKEY.getValue(), QUALITY_ASSURANCE.getValue())
                .selectFirstAvailableJobOffer();

        new JobOfferPage()
                 .verifyPageIsOpen()
                 .verifyTitle(QUALITY_ASSURANCE.getValue())
                 .verifyDescription()
                 .verifyRequirements()
                 .verifyApplicationButtonAndClick();

        new JobApplicationPage()
                .verifyJobApplicationPageIsOpen();
    }
}
