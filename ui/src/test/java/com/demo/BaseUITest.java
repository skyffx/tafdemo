package com.demo;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Selenide.*;

@Slf4j
@Listeners({ScreenShooter.class})
public class BaseUITest {

    @BeforeSuite
    public void setUp() {
        String browser = System.getProperty("browser");
        if (browser.equals("chrome")) {
            Configuration.browser = "chrome";
            Configuration.browserVersion = "88.0";
        } else if (browser.equals("firefox")) {
            Configuration.browser = "firefox";
            Configuration.browserVersion = "84.0";
        } else {
            log.error("Web browser not found!");
            System.exit(0);
        }

        Configuration.fastSetValue = true;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
    }

    @AfterSuite
    public void tearDown() {
        WebDriverRunner.clearBrowserCache();
        WebDriverRunner.closeWebDriver();
    }

    @Step("Open Insider main page")
    protected void openInsiderMainPage() {
        open("https://useinsider.com/");
    }

    public String getCurrentUrl() {
        return WebDriverRunner.getWebDriver().getCurrentUrl();
    }
}
