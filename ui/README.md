## UI

UI Test Automation Demo 

### Setup docker infrastructure
- `cd ui`
- `docker pull selenoid/vnc_chrome:88.0`
- `docker pull selenoid/vnc_firefox:84.0`
- `docker pull selenoid/video-recorder:latest-release`
- `docker-compose up` (run in git bash)
- open http://localhost:8082/#/

### Run test
##### with Chrome
- `cd ui`
- `mvn -Dtest=JobApplicationTest -Dbrowser=chrome clean test`
##### with Firefox
- `cd ui`
- `mvn -Dtest=JobApplicationTest -Dbrowser=firefox clean test`

#### Screenshots from failed steps can be found in
`/target/reports/...`

### Generate report
- `mvn allure:serve`

</br><p align="center">
	<img src="report.png" />
</p>